using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CrashDetector : MonoBehaviour
{
    [SerializeField] float CrashReloadSceneDelay = 1f;
    [SerializeField] ParticleSystem CrashEffect;
    [SerializeField] AudioClip CrashSoundFX;

    bool hasCrashed = false;

    void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.tag == "Ground" && !hasCrashed)
        {
            hasCrashed = true;
            FindObjectOfType<PlayerController>().DisableControls();
            CrashEffect.Play();
            GetComponent<AudioSource>().PlayOneShot(CrashSoundFX);
            Invoke("CrashReloadScene", CrashReloadSceneDelay);
        }    
    }

    void CrashReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
